Neural Probabilistic Language Model Toolkit -- Java
===================================================

This is based on the [NPLM toolkit](http://nlg.isi.edu/software/nplm/) from ISI. In fact, it
is a simple port of the nplm.py python file from that distribution. The goal is to have a
reader for the language model file which can be used in Java. The interesting part is the
matrix multiplication to get the probability of text.

Hopefully there will be a JNI section for training a LM from Java also.

To train a model, use all the tools available from the NPLM toolkit (not in Java). These are executables
called `prepareNeuralNetwork`, `trainNeuralNetwork`, `testNeuralNetwork`, `testNeuralLM`, etc.

