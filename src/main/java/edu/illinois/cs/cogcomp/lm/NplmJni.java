package edu.illinois.cs.cogcomp.lm;

/**
 * Created by mayhew2 on 11/18/15.
 */
public class NplmJni {

    static {
        System.loadLibrary("nplm");
    }

    private native void sayHello();

    public static void main(String[] args){
        new NplmJni().sayHello();
    }

}
