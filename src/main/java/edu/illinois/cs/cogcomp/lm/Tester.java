package edu.illinois.cs.cogcomp.lm;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.lm.NeuralLM;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mayhew2 on 11/19/15.
 */
public class Tester {

    public static void main(String[] args) throws Exception {

        NeuralLM m = NeuralLM.from_file("/home/mayhew2/IdeaProjects/illinois-transliteration/lm/nplm-ru.txt");

//        List<String> ngrams = new ArrayList<>();

        //ngrams.add("т");
        //ngrams.add("и");
        //ngrams.add("н");

//        ngrams.add("a");
//        ngrams.add("b");
//        ngrams.add("н");

        List<String> lines = LineIO.read("/home/mayhew2/IdeaProjects/illinois-transliteration/lm/ruwords.txt");

        double total = 0;

        List<String> ngrams = new ArrayList<>();

        for(String line : lines){
            line = "<s> " + line;
            String[] sline = line.split(" ");

            for(int i = 0; i < sline.length-1; i++){
                ngrams.clear();

                ngrams.add(sline[i]);
                ngrams.add(sline[i+1]);


                total += m.ngram_prob(ngrams);
            }


        }
        System.out.println(total);

        System.out.println();
        String russian = "хрусталь";

        for(char c : russian.toCharArray()){
            ngrams.clear();
            ngrams.add(c + "");
            ngrams.add("</s>");

            System.out.println(m.ngram_prob(ngrams));
        }




    }

}
